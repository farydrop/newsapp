package com.example.myapplication.utils

const val API_KEY = "350dd8ccf49c473091d029790596b996"
const val BASE_URL = "https://newsapi.org"
const val QUERY_PAGE_SIZE = 20
const val SEARCH_NEWS_TIME_DELAY = 500L
const val PREFETCH_DISTANCE = 1