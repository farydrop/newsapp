package com.example.myapplication

import android.app.Application
import javax.inject.Inject

class NewsApplication @Inject constructor() : Application()