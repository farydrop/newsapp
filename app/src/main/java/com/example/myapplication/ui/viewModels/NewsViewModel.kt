package com.example.myapplication.ui.viewModels

import androidx.lifecycle.*
import androidx.paging.*
import com.example.myapplication.data.repository.NewsRepositoryImpl
import com.example.myapplication.domain.models.*
import com.example.myapplication.domain.usecase.NewsUseCases
import com.example.myapplication.utils.PREFETCH_DISTANCE
import com.example.myapplication.utils.QUERY_PAGE_SIZE
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val TAG = "NewsViewModel"

// TODO tests

@HiltViewModel
class NewsViewModel @Inject constructor(
    private val newsRepositoryImpl: NewsRepositoryImpl,
    private val newsUseCases: NewsUseCases
) : ViewModel() {
    val breakingNews: StateFlow<PagingData<ArticleDomain>> = Pager<Int, ArticleDomain>(
        PagingConfig(
            pageSize = QUERY_PAGE_SIZE,
            initialLoadSize = QUERY_PAGE_SIZE,
            prefetchDistance = PREFETCH_DISTANCE,
            enablePlaceholders = true
        )
    ) {
        newsRepositoryImpl.createBreakingNewsPageSource()
    }.flow
        .cachedIn(viewModelScope)
        .stateIn(viewModelScope, SharingStarted.Lazily, PagingData.empty())

    var currentSavedPagingSource: SavedNewsPageSource? = null

    val savedNews: StateFlow<PagingData<ArticleDomain>> = Pager<Int, ArticleDomain>(
        PagingConfig(
            pageSize = QUERY_PAGE_SIZE,
            initialLoadSize = QUERY_PAGE_SIZE,
            prefetchDistance = PREFETCH_DISTANCE,
            enablePlaceholders = true
        )
    ) {
        newsRepositoryImpl.createSavedNewsPageSource().also { currentSavedPagingSource = it }
    }.flow
        .cachedIn(viewModelScope)
        .stateIn(viewModelScope, SharingStarted.Lazily, PagingData.empty())

    private val _query = MutableStateFlow("")
    val query: StateFlow<String> = _query.asStateFlow()

    private val newsEventsChannel = Channel<NewsEvent>()
    val newsEvent = newsEventsChannel.receiveAsFlow()


    fun onNewsSelected(article: ArticleDomain) {
        viewModelScope.launch {
            val param = NewsSelectedParam(
                newsEventsChannel = newsEventsChannel,
                article = article
            )
            newsUseCases.newsSelectedUseCase(param)
        }
    }



    fun setQuery(query: String) {
        val param = SaveQueryParams(_query = _query, query = query)

        newsUseCases.saveQueryUseCase(saveQueryParams = param)
    }

    sealed class NewsEvent {
        data class ShowToastMessage(val msgId: Int): NewsEvent()
        data class NavigateToArticleScreen(val article: ArticleDomain): NewsEvent()
        data class ShowArticleDeletedSnackbar(
            val msgId: Int,
            val actonMsgId: Int,
            val article: ArticleDomain
        ): NewsEvent()
    }
}
