package com.example.myapplication.data.repository

import com.example.myapplication.data.api.BreakingNewsPageSource
import com.example.myapplication.data.api.EverythingNewsPageSource
import com.example.myapplication.data.api.NewsAPI
import com.example.myapplication.data.api.db.ArticleDatabase
import com.example.myapplication.domain.models.ArticleDomain
import com.example.myapplication.domain.repository.NewsRepository
import com.example.myapplication.utils.mapToArticle
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
abstract class NewsRepositoryImpl @Inject constructor(
    val db: ArticleDatabase,
    val api: NewsAPI
): NewsRepository {

    override suspend fun upsert(article: ArticleDomain): Long =
        db.getArticleDao().upsert(article.mapToArticle())

    override suspend fun deleteArticle(article: ArticleDomain) =
        db.getArticleDao().deleteArticle(article.mapToArticle())


    override fun createBreakingNewsPageSource() = BreakingNewsPageSource(
        newsApi = api
    )

    override fun createEverythingNewsPageSource(query: String) = EverythingNewsPageSource(
        newsApi = api,
        query = query
    )

}
