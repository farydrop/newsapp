package com.example.myapplication.data.models

import java.io.Serializable

data class Source(
    val id: Any,
    val name: String
): Serializable