package com.example.myapplication.domain.usecase

import javax.inject.Inject

data class NewsUseCases @Inject constructor(
    val saveArticleUseCase: SaveArticleUseCase,
    val deleteArticleUseCase: DeleteArticleUseCase,
    val newsSelectedUseCase: NewsSelectedUseCase,
    val saveQueryUseCase: SaveQueryUseCase
)