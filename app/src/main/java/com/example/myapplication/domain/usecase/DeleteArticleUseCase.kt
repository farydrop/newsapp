package com.example.myapplication.domain.usecase

import com.example.myapplication.R
import com.example.myapplication.domain.models.DeleteArticleParam
import com.example.myapplication.domain.repository.NewsRepository
import com.example.myapplication.ui.viewModels.NewsViewModel
import javax.inject.Inject

class DeleteArticleUseCase @Inject constructor(
    private val newsRepositoryImpl: NewsRepository
) {

    suspend operator fun invoke(
        deleteArticleParam: DeleteArticleParam
    ) {
        newsRepositoryImpl.deleteArticle(deleteArticleParam.article)
        deleteArticleParam.currentSavedPagingSource?.invalidate()
        deleteArticleParam.newsEventsChannel.send(
            NewsViewModel.NewsEvent.ShowArticleDeletedSnackbar(
                R.string.delete_article_snackbar_msg,
                R.string.delete_article_snackbar_action,
                deleteArticleParam.article
            )
        )
    }
}