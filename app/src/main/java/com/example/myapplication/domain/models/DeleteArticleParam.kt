package com.example.myapplication.domain.models

import com.example.myapplication.ui.viewModels.NewsViewModel
import kotlinx.coroutines.channels.Channel

data class DeleteArticleParam(
    val article: ArticleDomain,
    val newsEventsChannel: Channel<NewsViewModel.NewsEvent>,
    val currentSavedPagingSource: SavedNewsPageSource?
)