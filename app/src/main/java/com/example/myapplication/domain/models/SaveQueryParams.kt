package com.example.myapplication.domain.models

import kotlinx.coroutines.flow.MutableStateFlow

data class SaveQueryParams(
    val _query: MutableStateFlow<String>,
    val query: String
)