package com.example.myapplication.domain.usecase

import com.example.myapplication.domain.repository.NewsRepository
import javax.inject.Inject

class SaveArticleUseCase @Inject constructor(
    private val newsRepositoryImpl: NewsRepository
) {

    suspend operator fun invoke(
        saveArticleParam: SaveArticleParam
    ) {
        newsRepositoryImpl.upsert(saveArticleParam.article)
        saveArticleParam.currentSavedPagingSource?.invalidate()
    }
}