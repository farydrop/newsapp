package com.example.myapplication.domain.repository

import com.example.myapplication.data.api.BreakingNewsPageSource
import com.example.myapplication.data.api.EverythingNewsPageSource
import com.example.myapplication.domain.models.ArticleDomain

interface NewsRepository {
    suspend fun upsert(article: ArticleDomain): Long

    suspend fun deleteArticle(article: ArticleDomain)

    fun createBreakingNewsPageSource(): BreakingNewsPageSource

    fun createEverythingNewsPageSource(query: String): EverythingNewsPageSource

    fun createSavedNewsPageSource(): SavedNewsPageSource
}