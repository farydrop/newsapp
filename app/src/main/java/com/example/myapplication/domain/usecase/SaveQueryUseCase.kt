package com.example.myapplication.domain.usecase

import com.example.myapplication.domain.models.SaveQueryParams
import javax.inject.Inject

class SaveQueryUseCase @Inject constructor() {

    operator fun invoke(
        saveQueryParams: SaveQueryParams
    ) {
        saveQueryParams._query.tryEmit(saveQueryParams.query)
    }
}