package com.example.myapplication.domain.usecase

import com.example.myapplication.domain.models.NewsSelectedParam
import com.example.myapplication.ui.viewModels.NewsViewModel
import javax.inject.Inject

class NewsSelectedUseCase @Inject constructor() {

    suspend operator fun invoke(
        newsSelectedParam: NewsSelectedParam
    ) {
        newsSelectedParam.newsEventsChannel.send(
            NewsViewModel.NewsEvent.NavigateToArticleScreen(
                newsSelectedParam.article
            )
        )
    }
}