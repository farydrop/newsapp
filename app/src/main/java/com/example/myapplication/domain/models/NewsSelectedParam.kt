package com.example.myapplication.domain.models

import com.example.myapplication.ui.viewModels.NewsViewModel
import kotlinx.coroutines.channels.Channel

data class NewsSelectedParam(
    val newsEventsChannel: Channel<NewsViewModel.NewsEvent>,
    val article: ArticleDomain
)
